#!/bin/sh


CHROOT_DIR="tmp"

DEBIAN_VERSION="bullseye"
ROOTFS_FILE="rootfs.tar.xz"
ROOTFS_URL="https://raw.githubusercontent.com/debuerreotype/docker-debian-artifacts/dist-arm64v8/${DEBIAN_VERSION}/slim/${ROOTFS_FILE}"


# Set Debian env
export DEBIAN_FRONTEND=noninteractive

# Download Debian rootfs
if [ ! -f $ROOTFS_FILE ]; then
  wget -q $ROOTFS_URL
  wget -q $ROOTFS_URL.sha256
fi

# Verify checksum
if ! grep -q $ROOTFS_FILE $ROOTFS_FILE.sha256; then
  sed -i '1 s/$/  '"$ROOTFS_FILE"'/' $ROOTFS_FILE.sha256
fi

sha256sum -c $ROOTFS_FILE.sha256
if [ $? -ne 0 ]; then
  echo "Malformed file"
  exit 1
fi

# Create chroot directory
mkdir $CHROOT_DIR

# Unpack Debian rootfs
tar xf $ROOTFS_FILE -C $CHROOT_DIR

# Mount pseudo filesystems
mount --bind /dev $CHROOT_DIR/dev
mount --bind /dev/pts $CHROOT_DIR/dev/pts
mount --bind /sys $CHROOT_DIR/sys
mount --bind /proc $CHROOT_DIR/proc

# Set DNS server
echo "nameserver 1.1.1.1" > $CHROOT_DIR/etc/resolv.conf

# Set hostname
chroot $CHROOT_DIR hostname rpi

# Install packages
chroot $CHROOT_DIR apt update
chroot $CHROOT_DIR apt install bc bison flex gcc libc6-dev libssl-dev make patch wget wireless-regdb xz-utils -y
