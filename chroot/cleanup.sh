#!/bin/sh


CHROOT_DIR="tmp"


# Umount chroot psuedo filesystems
umount $CHROOT_DIR/dev/pts
umount $CHROOT_DIR/dev
umount $CHROOT_DIR/proc
umount $CHROOT_DIR/sys

# Remove chroot
rm -rf $CHROOT_DIR
