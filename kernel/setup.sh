#!/bin/sh


CHROOT_DIR="tmp"

KERNEL_VERSION="5.10.170"
KERNEL_FILE="linux-${KERNEL_VERSION}.tar.xz"
KERNEL_URL="https://cdn.kernel.org/pub/linux/kernel/v5.x/${KERNEL_FILE}"


# Get Linux kernel archive
chroot $CHROOT_DIR wget -q $KERNEL_URL

# Unpack the kernel archive
chroot $CHROOT_DIR tar xf $KERNEL_FILE

# Patch the kernel
chroot $CHROOT_DIR patch -d linux-$KERNEL_VERSION -p0 < kernel/linux.patch

# Copy BCM43455 firmware
mkdir -p $CHROOT_DIR/lib/firmware/brcm
cp -r kernel/firmware/brcm $CHROOT_DIR/lib/firmware

# Copy .config file
cp kernel/.config tmp/linux-${KERNEL_VERSION}

# Compile the kernel image and device tree
chroot $CHROOT_DIR make -C linux-${KERNEL_VERSION} ARCH=arm64 Image
chroot $CHROOT_DIR make -C linux-${KERNEL_VERSION} ARCH=arm64 dtbs

# Copy kernel image
cp $CHROOT_DIR/linux-${KERNEL_VERSION}/arch/arm64/boot/Image boot/kernel8.img

# Copy dtb
cp $CHROOT_DIR/linux-${KERNEL_VERSION}/arch/arm64/boot/dts/broadcom/bcm2711-rpi-4-b.dtb boot
