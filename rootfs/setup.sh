#!/bin/sh


CHROOT_DIR="tmp"

EIWD_FILE="master.tar.gz"
EIWD_URL="https://codeberg.org/jkm/eiwd/archive/${EIWD_FILE}"


# Get eiwd archive
chroot $CHROOT_DIR wget -q $EIWD_URL

# Unpack eiwd archive
chroot $CHROOT_DIR tar xf $EIWD_FILE

# Compile eiwd binary
chroot $CHROOT_DIR make -C eiwd
chroot $CHROOT_DIR make -C eiwd install
