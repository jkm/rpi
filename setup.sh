#!/bin/sh


CHROOT_DIR="tmp"
MOUNT_DIR="/mnt"

ROOTFS_FILE="rootfs.tar.xz"
RPI_FIRMWARE_COMMIT="master"
RPI_FIRMWARE_URL="https://raw.githubusercontent.com/raspberrypi/firmware/${RPI_FIRMWARE_COMMIT}/boot"


# Detach all mounted images
losetup -D

# Remove old image
rm -f rpi.img

# Setup image file
dd if=/dev/zero of=rpi.img bs=512 count=344064 status=none

# Attach image to loop device
losetup -P /dev/loop0 rpi.img

# Partition disk image
sfdisk -q /dev/loop0 < disk/layout

# Make filesystems
mkfs.vfat /dev/loop0p1
mkfs.ext4 -q /dev/loop0p2

# Mount boot partition
mount /dev/loop0p1 $MOUNT_DIR

# Copy boot files
cp -r boot/* $MOUNT_DIR

# Download RPi firmware
wget -q $RPI_FIRMWARE_URL/fixup4.dat -O $MOUNT_DIR/fixup4.dat
wget -q $RPI_FIRMWARE_URL/start4.elf -O $MOUNT_DIR/start4.elf

# Touch all files
find $MOUNT_DIR | xargs touch -h

# Umount boot partition
umount /dev/loop0p1

# Mount rootfs volume
mount /dev/loop0p2 $MOUNT_DIR

# Unpack Debian rootfs
tar xf $ROOTFS_FILE -C $MOUNT_DIR

# Remove /etc/motd
rm $MOUNT_DIR/etc/motd

# Remove systemd orphans
rm -rf $MOUNT_DIR/lib/systemd
rm -rf $MOUNT_DIR/var/lib/systemd

# Create /etc/dropbear directory
mkdir $MOUNT_DIR/etc/dropbear

# Create /root/.ssh directory
mkdir $MOUNT_DIR/root/.ssh

# Create eiwd directory
mkdir $MOUNT_DIR/var/lib/eiwd

# Copy eiwd binary
cp $CHROOT_DIR/usr/bin/eiwd $MOUNT_DIR/usr/bin/eiwd

# Copy .psk files
if [ -f rootfs/*.psk ]; then
  cp rootfs/*.psk $MOUNT_DIR/var/lib/eiwd
fi

# Copy authorized_keys file
if [ -f $HOME/.ssh/authorized_keys ]; then
  cp $HOME/.ssh/authorized_keys $MOUNT_DIR/root/.ssh
fi

# Set hostname
echo rpi > $MOUNT_DIR/etc/hostname

# Set default DNS server
echo "nameserver 1.1.1.1" > $MOUNT_DIR/etc/resolv.conf

# Update packages
chroot $MOUNT_DIR apt update

# Install packages
chroot $MOUNT_DIR apt -y install \
			dropbear-bin \
			fdisk \
			htop \
			inetutils-ping \
			iproute2 \
			less \
			picocom \
			procps \
			psmisc \
			sntp \
			sysvinit-core \
			tmux \
			usbutils \
			vim-tiny

# Remove /lib/init
rm -rf $MOUNT_DIR/lib/init

# Tidy up /etc directory
rm -rf $MOUNT_DIR/etc/rc*
rm -rf $MOUNT_DIR/etc/default
rm -rf $MOUNT_DIR/etc/iproute2
rm -rf $MOUNT_DIR/etc/systemd
rm $MOUNT_DIR/etc/init.d/*
rm $MOUNT_DIR/etc/init.d/.depend.*

# Remove /usr/share/doc
rm -rf $MOUNT_DIR/usr/share/doc/*

# Remove /usr/share/man
rm -rf $MOUNT_DIR/usr/share/man/*

# Remove /usr/share/locale
rm -rf $MOUNT_DIR/usr/share/locale/*

# Remove unused sysvinit binaries
rm $MOUNT_DIR/sbin/poweroff
rm $MOUNT_DIR/sbin/reboot
rm $MOUNT_DIR/sbin/telinit

# Remove man links
rm $MOUNT_DIR/etc/alternatives/*.gz

# Copy necessary files
cp etc/inittab $MOUNT_DIR/etc/inittab
cp etc/init.d/rcS $MOUNT_DIR/etc/init.d/rcS

# Touch all files
find $MOUNT_DIR | xargs touch -h

# Umount rootfs volume
umount /dev/loop0p2

# Detach image from loop device
losetup -d /dev/loop0

# Copy image to given block device
if [ ! -z $1 ]; then
  dd if=rpi.img of=$1 bs=4M status=none
fi
